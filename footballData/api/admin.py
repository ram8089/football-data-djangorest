from django.contrib import admin
from api.models import Competition, Team, Player

admin.site.register(Competition)
admin.site.register(Team)
admin.site.register(Player)