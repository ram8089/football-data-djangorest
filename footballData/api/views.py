from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import requests

from api.models import Team, Player, Competition
from api.serializers import CompetitionSerializer, TeamSerializer, PlayerSerializer

MESSAGES = {
    status.HTTP_201_CREATED: "Successfully imported",
    status.HTTP_400_BAD_REQUEST: "Bad request",
    status.HTTP_404_NOT_FOUND: "Not found",
    status.HTTP_409_CONFLICT: "League already imported",
    status.HTTP_504_GATEWAY_TIMEOUT: "Server Error",
}


class CompetitionDetail(APIView):
    """
    Descripcion de una Competition
    """
    _api_key = '4cb19628e52a4a3992ba1271e70ed174'
    _headers = {'X-Auth-Token': _api_key}
    _base_url = 'http://api.football-data.org/v2/'

    def get_competition_teams(self, url):
        req = requests.get(self._base_url + url, headers=self._headers)
        if req.status_code == requests.codes.ok:
            # Procesando los datos crudos
            data_raw = req.json()
            for index, team in enumerate(data_raw['teams']):
                team['area'] = team['area']['name']
                data_raw['teams'][index] = team
            
            # Datos procesados
            data = dict(
                name = data_raw['competition']['name'],
                code = data_raw['competition']['code'],
                area = data_raw['competition']['area']['name'],
                teams = data_raw['teams']
            )
            serializer = CompetitionSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = status.HTTP_201_CREATED
            else:
                # Nota: Existen teams con campos incorrectos
                print(serializer.errors)
                response = status.HTTP_400_BAD_REQUEST
        elif req.status_code == 404:
            response = status.HTTP_404_NOT_FOUND
        else:
            response = status.HTTP_504_GATEWAY_TIMEOUT
        return response

    def get_players(self, code):
        # Obtener la lista de Teams para 'code'
        teams = Team.objects.filter(competition__code=code)

        # Para cada Team obtener lista de Players
        for team in teams:
            team_id = str(team.id)
            url = self._base_url + 'teams/' + team_id
            req = requests.get(url, headers=self._headers)
            print(url + ' ' + str(req.status_code))
            if req.status_code == requests.codes.ok:
                data = req.json()
                players = data['squad']
                
                # Se guarda el Player
                for player in players:
                    serializer = PlayerSerializer(
                        data=player, context={'team_id': team_id})
                    if serializer.is_valid():
                        serializer.save()
                        response = status.HTTP_201_CREATED
                    else:
                        response = status.HTTP_400_BAD_REQUEST
            elif req.status_code == 404:
                response = status.HTTP_404_NOT_FOUND
            else:
                response = status.HTTP_504_GATEWAY_TIMEOUT
        return response

    def get(self, request, code, format=None):
        try:
            # Si la competicion existe no se pide a la api
            competition = Competition.objects.get(code=code)
            serializer = CompetitionSerializer(competition)
            return Response(serializer.data, status=status.HTTP_409_CONFLICT)
        except Competition.DoesNotExist:
            # Se hacen las consultas secuencialmente
            response = self.get_competition_teams(
                'competitions/{code}/teams'.format(code=code))
            if response == status.HTTP_201_CREATED:
                response = self.get_players(code)
            return Response({'message': MESSAGES[response]}, status=response)
    
    def get_object(self, code):
        try:
            return Competition.objects.get(code=code)
        except Competition.DoesNotExist:
            raise Http404

    def delete(self, request, code, format=None):
        competition = self.get_object(code)
        teams = competition.teams.all()
        for team in teams:
            team.delete()
        competition.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TeamDetail(APIView):
    """
    Descripcion de un Team
    """
    _api_key = '4cb19628e52a4a3992ba1271e70ed174'
    _headers = {'X-Auth-Token': _api_key}
    _base_url = 'http://api.football-data.org/v2/'

    def get_teams(self, team_id):
        url = self._base_url + 'teams/' + str(team_id)
        req = requests.get(url, headers=self._headers)
        if req.status_code == requests.codes.ok:
            # Procesando los datos crudos
            data = req.json()
            data['area'] = data['area']['name']
            players = data['squad']

            # Se guarda el team
            serializer = TeamSerializer(data=data)
            if serializer.is_valid():
                serializer.save()

                # Se guardan los players
                for player in players:
                    serializer = PlayerSerializer(
                        data=player, context={'team_id': team_id})
                    if serializer.is_valid():
                        serializer.save()
                        response = status.HTTP_201_CREATED
                    else:
                        print(serializer.errors)
                        response = status.HTTP_400_BAD_REQUEST
                    response = status.HTTP_201_CREATED
            else:
                print(serializer.errors)
                response = status.HTTP_400_BAD_REQUEST         
        elif req.status_code == 404:
            response = status.HTTP_404_NOT_FOUND
        else:
            response = status.HTTP_504_GATEWAY_TIMEOUT
        return response

    def get(self, request, id, format=None):
        try:
            # Si el team existe no se pide a la api
            team = Team.objects.get(id=id)
            serializer = TeamSerializer(team)
            return Response(serializer.data, status=status.HTTP_409_CONFLICT)
        except Team.DoesNotExist:
            # Se hacen las consultas secuencialmente
            response = self.get_teams(id)
            return Response({'message': MESSAGES[response]}, status=response)


class TotalPlayers(APIView):
    """
    Numero total de players por competencia
    """
    def get_total_players(self, code, format=None):
        total_players = 0
        teams = Team.objects.filter(competition__code=code)
        for team in teams:
            players = team.player_set.all()
            total_players += players.count()
        return total_players

    def get(self, request, code, format=None):
        try:
            # Si existe la competicion calculamos el total de players
            Competition.objects.get(code=code)
            total_players = self.get_total_players(code)
            return Response({'total': total_players}, status=status.HTTP_200_OK)
        except Competition.DoesNotExist:
            response = status.HTTP_404_NOT_FOUND
            return Response({'message': MESSAGES[response]}, status=response)
