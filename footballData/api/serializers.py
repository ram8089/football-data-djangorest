from rest_framework import serializers
from api.models import Competition, Team, Player


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = [
            'name', 
            'position', 
            'dateOfBirth', 
            'countryOfBirth',
            'nationality'
        ]
    
    def create(self, validated_data):
        team_id = self.context.get("team_id")
        team = Team.objects.get(id=team_id)
        player = Player.objects.create(team=team, **validated_data)
        return player


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = [
            'id',
            'name', 
            'tla', 
            'shortName', 
            'area', 
            'email'
        ]


class CompetitionSerializer(serializers.ModelSerializer):
    teams = TeamSerializer(many=True)

    class Meta:
        model = Competition
        fields = [
            'name', 
            'code', 
            'area', 
            'teams'
        ]
    
    def create(self, validated_data):
        teams_data = validated_data.pop('teams')
        competition = Competition.objects.create(**validated_data)
        for team_data in teams_data:
            team = Team.objects.create(**team_data)
            competition.teams.add(team)
        return competition