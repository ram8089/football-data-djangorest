## Football Data with Django Rest

## Introduccion

El proyecto en cuestión representa la breve implementancion del siguiente
enunciado usando la tecnología de Django y Django Rest:

## Task

The goal is to make a **project** that exposes an API with an HTTP GET in this URI: `/import-league/{leagueCode}` . E.g., it must be possible to invoke the service using this URL:  
`[http://localhost](http://localhost/):<port>/import-league/CL`

The service implementation must get data using the given `{leagueCode}`, by making requests to the [**`http://www.football-data.org/`**](http://www.football-data.org/)API _(you can see the documentation entering to the site, use the API v2)_, and **import** the data into a **DB** _(MySQL is suggested, but you can use any DB of your preference)._ The data requested is:

`Competition ("name", "code", "areaName")`

`Team ("name", "tla", "shortName", "areaName", "email")`

`Player("name", "position", "dateOfBirth", "countryOfBirth", "nationality")`

Feel free to add to this data structure any other field that you might need (for the foreign keys relationship).

Additionally, expose an HTTP GET in URI `/total-players/{leagueCode}` , with a simple JSON response like this:  
`{"total" : _**N**_ }` and HTTP Code **200**.

where `_**N**_` is the total amount of players belonging to all teams that participate in the given league (`leagueCode`). This service must rely exclusively on the data saved inside the DB (it **must not** access the API _[football-data.org](http://football-data.org/)_). If the given leagueCode is not present into the DB, it should respond an HTTP Code **404**.

### Remarks

-   You are allowed to use any library related to the language in which you are implementing the project.
-   You must provide the SQL for data structure creation; it is a plus that the project automatically creates the structure (if it doesn't exist) when it runs the first time.
-   All the mentioned DB entities must keep their proper relationships (the players with which team they belong to; the teams in which leagues participate).


-   The API responses for `/import-league/{leagueCode}` are:
    -   HttpCode **201**, `{"message": "Successfully imported"}` --> When the `leagueCode` was successfully imported.
    -   HttpCode **409**, `{"message": "League already imported"}` --> If the given `leagueCode` was already imported into the DB (and in this case, it doesn't need to be imported again).
    -   HttpCode **404**, `{"message": "Not found" }` --> if the `leagueCode` was not found.
    -   HttpCode **504**, `{"message": "Server Error"` } --> If there is any connectivity issue either with the football API or the DB server.

-   It might happen that when a given `leagueCode` is being imported, the league has participant teams that are already imported (because each team might belong to one or more leagues). For these cases, it **must** add the relationship between the league and the team(s) (and omit the process of the preexistent teams and their players). 

## Project Setup

Descargar el repositorio con

```
$ git clone https://gitlab.com/juanpgomez/football-data-djangorest.git
```
En la carpeta raiz del repo crear el virtualenv y levantarlo:

```
$ cd football-data-djangorest/
$ virtualenv env
$ source env/bin/activate
(env)$ pip install -r requirements.txt
```
A continuación generamos el proyecto con el comando:
```
(env)$ django-admin startproject footballData
```
Y terminamos instalando la aplicación para la api:
```
(env)$ cd footballData
(env)$ python manage.py startapp api
```

Ahora tenemos que configurar nuestro proyecto Django para hacer uso de REST Framework.
Primero agregue la aplicación `api` y `rest_framework` a la sección `INSTALLED_APPS` dentro de _footballData/footballData/settings.py_:

```
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'api',
    'rest_framework'
]
```
## Database Setup

Vamos a configurar la base de datos de MySql y aplicarle todas las migraciones.

> **NOTA**: Siéntete libre de cambiar la base de datos MySql por cualquiera de tu preferencia!

### Install MySQL Database Connector

In order to use MySQL with our project, we will need a Python 3 database connector library compatible with Django. So, we will install the database connector,  `mysqlclient`, which is a forked version of  `MySQLdb`.

According to the  `mysqlclient`  documentation, “`MySQLdb`  is a thread-compatible interface to the popular  `MySQL`  database server that provides the Python database API.” The main difference being that  `mysqlclient`  has the added benefit of including Python 3 support.

First thing we will need to do is install  `python3-dev`. You can install  `python3-dev`  by running the following command:

```
(env)$ sudo apt-get install python3-dev
```

Once  `python3-dev`  is installed, we can install the necessary Python and MySQL development headers and libraries:

```
(env)$ sudo apt-get install python3-dev libmysqlclient-dev
```

When you see the following output:

```
Output:
After this operation, 11.9 MB of additional disk space will be used.
Do you want to continue? [Y/n]
```

Enter  `y`  then hit  `ENTER`  to continue.

Then, we will use  `pip3`  to install the  `mysqlclient`  library from  `PyPi`. Since our version of  `pip`  points to  `pip3`, we can just use  `pip`.

```
(env)$ pip install mysqlclient
```

You will see output similar to this, verifying that it is installing properly:

```
Collecting mysqlclient
  Downloading mysqlclient-1.3.12.tar.gz (82kB)
    100% |████████████████████████████████| 92kB 6.7MB/s
Building wheels for collected packages: mysqlclient
  Running setup.py bdist_wheel for mysqlclient ... done
  Stored in directory: /root/.cache/pip/wheels/32/50/86/c7be3383279812efb2378c7b393567569a8ab1307c75d40c5a
Successfully built mysqlclient
Installing collected packages: mysqlclient
Successfully installed mysqlclient-1.3.12
```

Now, install  `MySQL`  server, with the following command:

```
(env)$ sudo apt-get install mysql-server
```

We have now successfully installed MySQL server and the MySQL client using the PyPi  `mysqlclient`  connector library.

### Create the Database
Now that the skeleton of your Django application has been set up and  `mysqlclient`  and  `mysql-server`  have been installed, we will to need to configure your Django backend for MySQL compatibility.

Verify that the MySQL service is running:
```
(env)$ systemctl status mysql.service
```
You will see output that looks similar to this:
```
mysql.service - MySQL Community Server
   Loaded: loaded (/lib/systemd/system/mysql.service; enabled; vendor preset: enabled)
   Active: active (running) since Sat 2017-12-29 11:59:33 UTC; 1min 44s ago
 Main PID: 26525 (mysqld)
   CGroup: /system.slice/mysql.service
        └─26525 /usr/sbin/mysqld

Dec 29 11:59:32 ubuntu-512mb-nyc3-create-app-and-mysql systemd[1]: Starting MySQL Community Server...
Dec 29 11:59:33 ubuntu-512mb-nyc3-create-app-and-mysql systemd[1]: Started MySQL Community Server.
```
If you instead see output similar to this:
```
mysqld.service
   Loaded: not-found (Reason: No such file or directory)
   Active: inactive (dead)
```
You can run  `sudo systemctl start mysql`  to get  `mysql.service`  started again.

Now you can log in with your MySQL credentials using the following command. Where  `-u`  is the flag for declaring your username and  `-p`  is the flag that tells MySQL that this user requires a password:

```
(env)$ mysql -u db_user -p
```

Then you will see output that asks you for this  db_user’s  password:

```
Enter password:
```

Once you enter your password correctly, you will see the following output:

```
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 6
Server version: 5.7.20-0ubuntu0.16.04.1 (Ubuntu)

Copyright (c) 2000, 2017, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

We can have MySQL show us the current databases with the following command:
```
mysql> SHOW DATABASES;
```
You’ll see output similar to the following, assuming that you haven’t created any databases yet:
```
+--------------------+
| Database          |
+--------------------+
| information_schema |
| mysql             |
| performance_schema |
| sys               |
+--------------------+
4 rows in set (0.00 sec)
```
By default, you will have 4 databases already created,  `information_schema`,  `MySQL`,  `performance_schema`  and  `sys`. We won’t need to touch these, as they contain information important for the MySQL server itself.

Now, that you’ve successfully logged into your MySQL server, we will create the initial database that will hold the data for our blog.

To create a database in MySQL run the following command, using a meaningful name for your database:
```
mysql> CREATE DATABASE football_data;
```
Upon successful creation of the database, you will see the following output:
```
Query OK, 1 row affected (0.00 sec)
```
Next, verify that the database is now listed in your list of available databases:
```
$ SHOW DATABASES;
```
You should see that the `football_data` database is among the databases included in the output:
```
+--------------------+
| Database          |
+--------------------+
| information_schema |
| football_data      |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec)
```
You’ve successfully created a MySQL database for your blog.

Whenever you’d like to exit MySQL server, press  `CTRL`  +  `D`.

### Add the MySQL Database Connection to your Application
Finally, we will be adding the database connection credentials to your Django application.

**Note:**  It is important to remember that connection settings, according to the Django documentation, are used in the following order:  
-  `OPTIONS`  
-  `NAME`,  `USER`,  `PASSWORD`,  `HOST`,  `PORT`  
-  `MySQL option files.`  

Let’s make the changes needed to connect your Django blog app to MySQL.

Navigate to the  `settings.py`  file and replace the current  `DATABASES`  lines with the following. We will configure your database dictionary so that it knows to use MySQL as your database backend and from what file to read your database connection credentials:

```
settings.py
...
# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': '/etc/mysql/my.cnf',
        },
    }
}
...
```
Next, let’s edit the config file so that it has your MySQL credentials. Use nano as `sudo` to edit the file and add the following information:
```
sudo nano /etc/mysql/my.cnf
```

```
my.cnf
...
[client]
database = db_name
user = db_user
password = db_password
default-character-set = utf8
```
Where database name in our case is  `football_data`, your username for the MySQL server is the one you’ve created, and the password is the MySQL server password you’ve created. Also, you’ll notice that  `utf8`  is set as the default encoding, this is a common way to encode unicode data in MySQL.

Once the file has been edited, we need to restart MySQL for the changes to take effect.
```
(env)$ systemctl daemon-reload
(env)$ systemctl restart mysql
```

### Test MySQL Connection to Application
We need to verify that the configurations in Django detect your MySQL server properly. We can do this by simply running the server. If it fails, it means that the connection isn’t working properly. Otherwise, the connection is valid.

We’ll need to navigate to the following directory:
```
(env)$ cd ~/football-data/footballData/
```
From there, we can run the following command:
```
python manage.py runserver
```
You will now see output similar to the following:
```
Performing system checks...

System check identified no issues (0 silenced).

You have 13 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): admin, auth, contenttypes, sessions.
Run 'python manage.py migrate' to apply them.

January 4, 2018 - 15:45:39
Django version 2.0.1, using settings 'blog.settings'
Starting development server at http://your-server-ip:8000/
Quit the server with CONTROL-C.
```
Follow the instructions from the output and follow the suggested link, `http://your-server-ip:8000/`, to view your web application and to verify that it is working properly.

### Make Migrations
With our models  `Competition`, `Team` and  `Player`  added, the next step is to apply these changes so that our  `MySQL`  database schema recognizes them and creates the necessary tables.

Let’s take a look at what tables already exist in our  `football_data`  database.

To do this, we need to log in to MySQL server.
```
(env)$ mysql football_data -u root
```
You’ll notice that if you type into the `SHOW DATABASES;` command, you’ll see the following:
```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| football_data      |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec)
```
We will be looking at the `football_data` database and view the tables that already exist, if any.
```
mysql> USE football_data;
```
Then, list the tables that exist in the `football_data` database:
```
mysql> SHOW TABLES;
```
```
Output:
Empty set (0.00 sec)
```
Right now it won’t show any tables because we haven’t made any migrations yet. But, when we do make migrations, it will display the tables that have been generated by Django.

Now we will proceed to make the migrations that apply the changes we’ve made in  `models.py`.

Close out of MySQL with  `CTRL`  +  `D`.

First, we must package up our model changes into individual migration files using the command  `makemigrations`. These files are similar to that of  `commits`  in a version control system like  `git`.

Now, if you navigate to  `~/football-data/footballData/api/migrations`  and run  `ls`, you’ll notice that there is only an  `__init__.py`  file. This will change once we add the migrations.

Change to the blog directory using  `cd`, like so:
```
(env)$ cd ~/football-data/footballData
```
```
(env)$ python manage.py makemigrations api
```
You should then see the following output in your terminal window:
```
Output:
Migrations for 'api':
  api/migrations/0001_initial.py
    - Create model Team
    - Create model Player
    - Create model Competition
```
Remember, when we navigated to  `~/football-data/footballData/api/migrations`  and it only had the  `__init__.py`  file? If we now  `cd`  back to that directory we’ll see that two things have been added,  `__pycache__`  and  `0001_initial.py`. The  `0001_initial.py`  file was automatically generated when you ran  `makemigrations`. A similar file will be generated every time you run  `makemigrations`.

Run  `less 0001_initial.py`  if you’d like to see what the file contains.

Now navigate to  `~/football-data/footballData`.

Since we have made a migration file, we must apply the changes these files describe to the database using the command  `migrate`. But first let’s see what current migrations exists, using the  `showmigrations`  command.
```
(env)$ python manage.py showmigrations
```
```
Output:
admin
 [X] 0001_initial
 [X] 0002_logentry_remove_auto_add
 [X] 0003_logentry_add_action_flag_choices
api
 [X] 0001_initial
auth
 [X] 0001_initial
 [X] 0002_alter_permission_name_max_length
 [X] 0003_alter_user_email_max_length
 [X] 0004_alter_user_username_opts
 [X] 0005_alter_user_last_login_null
 [X] 0006_require_contenttypes_0002
 [X] 0007_alter_validators_add_error_messages
 [X] 0008_alter_user_username_max_length
 [X] 0009_alter_user_last_name_max_length
 [X] 0010_alter_group_name_max_length
 [X] 0011_update_proxy_permissions
contenttypes
 [X] 0001_initial
 [X] 0002_remove_content_type_name
sessions
 [X] 0001_initial
```

You’ll notice the migration we’ve just added for  `api`, which contains the migration  `0001_initial`  for models  `Team`, `Player` and  `Competition`.

Now let’s see the  `SQL`  statements that will be executed once we make the migrations, using the following command. It takes in the migration and the migration’s title as an argument:
```
(env)$ python manage.py sqlmigrate api 0001_initial
```
As you see below, this is the actual SQL query being made behind the scenes.
```
BEGIN;
--
-- Create model Team
--
CREATE TABLE "api_team" ("id" integer unsigned NOT NULL PRIMARY KEY CHECK ("id" >= 0), "name" varchar(30) NOT NULL, "tla" varchar(3) NOT NULL, "shortName" varchar(30) NOT NULL, "area" varchar(30) NOT NULL, "email" varchar(254) NULL);
--
-- Create model Player
--
CREATE TABLE "api_player" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(30) NOT NULL, "position" varchar(30) NOT NULL, "dateOfBirth" datetime NOT NULL, "countryOfBirth" varchar(30) NOT NULL, "nationality" varchar(30) NOT NULL, "team_id" integer NOT NULL REFERENCES "api_team" ("id") DEFERRABLE INITIALLY DEFERRED);
--
-- Create model Competition
--
CREATE TABLE "api_competition" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "name" varchar(30) NOT NULL, "code" varchar(3) NOT NULL, "area" varchar(30) NOT NULL);
CREATE TABLE "api_competition_teams" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "competition_id" integer NOT NULL REFERENCES "api_competition" ("id") DEFERRABLE INITIALLY DEFERRED, "team_id" integer NOT NULL REFERENCES "api_team" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE INDEX "api_player_team_id_df5bf8ad" ON "api_player" ("team_id");
CREATE UNIQUE INDEX "api_competition_teams_competition_id_team_id_4f75f692_uniq" ON "api_competition_teams" ("competition_id", "team_id");
CREATE INDEX "api_competition_teams_competition_id_12e0517c" ON "api_competition_teams" ("competition_id");
CREATE INDEX "api_competition_teams_team_id_d61abb46" ON "api_competition_teams" ("team_id");
COMMIT;
```
Let’s now perform the migrations so that they get applied to our MySQL database.
```
(env)$ python manage.py migrate
```
We will see the following output:
```
Operations to perform:
  Apply all migrations: admin, api, auth, contenttypes, sessions
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying api.0001_initial... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying sessions.0001_initial... OK
```
You have now successfully applied your migrations.

It is important to keep in mind that there are 3 caveats to Django migrations with MySQL as your backend, as stated in the Django documentation.

-   Lack of support for transactions around schema alteration operations. In other words, if a migration fails to apply successfully, you will have to manually unpick the changes you’ve made in order to attempt another migration. It is not possible to rollback, to an earlier point, before any changes were made in the failed migration.
-   For most schema alteration operations, MySQL will fully rewrite tables. In the worst case, the time complexity be proportional to the number of rows in the table to add or remove columns. According to the Django documentation, this could be as slow as one minute per million rows.
-   In MySQL, there are small limits on name lengths for columns, tables and indices. There is also a limit on the combined size of all columns and index covers. While some other backends can support higher limits created in Django, the same indices will fail to be created with a MySQL backend in place.

For each database you consider for use with Django, be sure to weigh the advantages and disadvantages of each.

### Verify Database Schema
With migrations complete, we should verify the successful generation of the MySQL tables that we’ve created via our Django models.

To do this, run the following command in the terminal to log in to MySQL.
```
(env)$ mysql football_data -u root
```
Now show the databases that exist.
```
mysql> SHOW DATABASES;
```
Select our database `football_data`
```
mysql> USE football_data;
```
Then type the following command to view the tables.
```
mysql> SHOW TABLES;
```
You should see the following:
```
Output:
+----------------------------+
| Tables_in_football_data    |
+----------------------------+
| api_competition            |
| api_competition_teams      |
| api_player                 |
| api_team                   |
| auth_group                 |
| auth_group_permissions     |
| auth_permission            |
| auth_user                  |
| auth_user_groups           |
| auth_user_user_permissions |
| django_admin_log           |
| django_content_type        |
| django_migrations          |
| django_session             |
+----------------------------+
```
You’ll see `api_player`, `api_team` and `api_competition`. These are the models that we’ve made ourselves. Let’s validate that they contain the fields we’ve defined.
```
mysql> DESCRIBE api_competition;
```
```
Output:
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(30) | NO   |     | NULL    |                |
| code  | varchar(3)  | NO   |     | NULL    |                |
| area  | varchar(30) | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
```
We have verified that the database tables were successfully generated from our Django model migrations.

### Reset Migrations
The project is still in the development environment and you want to perform a full clean up. You don’t mind throwing the whole database away.

##### 1. Remove the all migrations files within your project

Go through each of your projects apps migration folder and remove everything inside, except the  `__init__.py`  file.

Or if you are using a unix-like OS you can run the following script (inside your project dir):

```
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc" -delete
```
##### 2. Drop the current database, or delete the  `db.sqlite3`  if it is your case.
##### 3. Create the initial migrations and generate the database schema:
```
python manage.py makemigrations 
python manage.py migrate
```
And you are good to go.
